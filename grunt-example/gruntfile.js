module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        concat: {
            basic: {
                src: [
                    './src/js/app.js',
                    './src/js/config.js'
                ],
                dest: './dist/main.js'
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');

    // Default task(s).
    grunt.registerTask('default', ['concat']);

};
