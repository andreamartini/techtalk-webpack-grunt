var app = window.app || {};
(function (config) {
    var App = app.App = function () {

    };

    App.prototype.init = function () {
        var button = document.getElementById(config.BUTTON_ID);
        button.addEventListener('click', this.play);
    };

    App.prototype.play = function () {
        var message = new SpeechSynthesisUtterance(config.WELCOME_TEXT);
        window.speechSynthesis.speak(message);
    };

    var appl = new App();
    appl.init();

})(app.Config);
