import {Config} from "./config";

export class App {

    constructor() {
        this.init();
    }

    init() {
        const button = document.getElementById(Config.BUTTON_ID);
        button.addEventListener('click', this.play);
    }

    play() {
        const message = new SpeechSynthesisUtterance(Config.WELCOME_TEXT);
        window.speechSynthesis.speak(message);
    }
}

new App();
